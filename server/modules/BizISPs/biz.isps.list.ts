import { Request, Response } from 'express';
const BizISPsModel = require("./../../models/bizisps.model");

class BizISPList {
  getAllBizISPs = (req: Request, res: Response) => {
    return new Promise((resolve, reject) => {
      BizISPsModel.find({}, { _id: 0 }).exec((err, collections) => {
        if (err) {
          reject(err);
        }
        let results = JSON.parse(JSON.stringify(collections));
        resolve({
          "payload": results.map((obj, index) => ({ ...obj, id: index }))
        });
      });
    });
  }
}
export const bizISPs = new BizISPList();