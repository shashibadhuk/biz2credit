import { Request, Response } from 'express';
const fs = require('fs')
const FILE_PATH = 'stats.json'

class APIHitTracker {
    getRoute(req) {
        const route = req.route ? req.route.path : '' // check if the handler exist
        const baseUrl = req.baseUrl ? req.baseUrl : '' // adding the base url if the handler is a child of another handler

        return route ? `${baseUrl === '/' ? '' : baseUrl}${route}` : 'unknown route'
    }
    dumpStats = (stats) => {
        try {
            fs.writeFileSync(FILE_PATH, JSON.stringify(stats), { flag: 'w+' })
        } catch (err) {
            console.error(err)
        }
    }
    readStats = () => {
        let result = {}
        try {
            result = JSON.parse(fs.readFileSync(FILE_PATH))
        } catch (err) {
            //console.error(err)
        }
        return result
    }
    getAPIHitCounts = (req: Request, res: Response) => {
        res.json(this.readStats())
    }
}
export const apiHitTracker = new APIHitTracker();