import * as express from 'express';
import { Application } from "express";
import { Promise } from 'bluebird';
import * as bodyParser from "body-parser";
var mongoose = require('mongoose');

//Imports Internal Modules
import { CONFIG } from "./config.default";
import { apiHitTracker } from "./modules/APIHit/tracker";
import { bizISPs } from "./modules/BizISPs/biz.isps.list";

const app: Application = express();

app.use(bodyParser.json());
app.use(express.static("assets"));

//Configure CORS & Headers
app.use((req, res, next) => {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods',
        'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers',
        'X-Requested-With,content-type,authorization');
    res.setHeader('Access-Control-Allow-Credentials', "true");
    // Pass to next layer of middleware
    next();
});

//Configure to maintain API hit counts
app.use((req, res, next) => {
    res.on('finish', () => {
        const stats = apiHitTracker.readStats()
        const event = `${req.method} ${apiHitTracker.getRoute(req)} ${res.statusCode}`
        const eventKey = event.replace(/[^a-zA-Z ]/g, "").replace(/\s/g, "").toLowerCase();
        stats[eventKey] = stats[eventKey] ? stats[eventKey] + 1 : 1
        apiHitTracker.dumpStats(stats)
    })
    next()
});

//MongoDB Connection
mongoose.Promise = Promise;
mongoose.connect(CONFIG.DATABASE.URL, CONFIG.DATABASE.OPTIONS).then(
    function () {
        console.log("\x1b[1m\x1b[32m%s\x1b[0m%s", "[MongoDB] : ", "CONNECTED (via Mongoose) on " + CONFIG.DATABASE.URL);
    },
    function (err) {
        console.log("\x1b[1m\x1b[32m%s\x1b[0m%s", "[MongoDB ERROR] : ", "ERROR NOT ABLE TO CONNECT TO MONGO (via Mongoose) " + JSON
            .stringify(err));
        console.log(CONFIG.DATABASE.URL);
        process.exit(1);
    });

//Register Routes
app.get('/api/bizisps', (request, response) => {
    bizISPs.getAllBizISPs(request, response).then((status) => {
        response.send(status);
    }, (err) => {
        response.send({
            "payload": []
        });
    });
});
app.route('/api/counts').get(apiHitTracker.getAPIHitCounts);

const httpServer: any = app.listen(9000, () => {
    console.log("HTTP REST API Server running at http://localhost:" + httpServer.address().port);
});

