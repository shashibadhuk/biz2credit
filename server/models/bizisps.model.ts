var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var BizISPsModel = new Schema({
  seq: {
    type: Number
  },
  name: {
    type: String
  },
  description: {
    type: String
  },
  logo: {
    type: String
  },
  averagerating: {
    type: String
  },
  contacts: {
    phone: {
      type: String
    },
    email: {
      type: String
    },
    website: {
      type: String
    }
  },
  plans: {
    type: Array
  }
}, {
  collection: 'BizISPs'
});
module.exports = mongoose.model('BizISPs', BizISPsModel);