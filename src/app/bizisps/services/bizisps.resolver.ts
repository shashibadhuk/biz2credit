import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {BizispEntityService} from './bizisp-entity.service';
import {filter, first, map, tap} from 'rxjs/operators';


@Injectable()
export class BizispsResolver implements Resolve<boolean> {

    constructor(private coursesService: BizispEntityService) {

    }

    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<boolean> {

        return this.coursesService.loaded$
            .pipe(
                tap(loaded => {
                    if (!loaded) {
                       this.coursesService.getAll();
                    }
                }),
                filter(loaded => !!loaded),
                first()
            );

    }

}
