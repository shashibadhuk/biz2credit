import { Injectable } from '@angular/core';
import { EntityCollectionServiceBase, EntityCollectionServiceElementsFactory } from '@ngrx/data';
import { BizISP } from '../model/bizisp';


@Injectable()
export class BizispEntityService
    extends EntityCollectionServiceBase<BizISP> {

    constructor(
        serviceElementsFactory:
            EntityCollectionServiceElementsFactory) {

        super('bizisp', serviceElementsFactory);

    }

}