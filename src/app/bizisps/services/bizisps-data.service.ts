import { Injectable } from '@angular/core';
import { DefaultDataService, HttpUrlGenerator } from '@ngrx/data';
import { BizISP } from '../model/bizisp';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';



@Injectable()
export class BizispsDataService extends DefaultDataService<BizISP> {


    constructor(http: HttpClient, httpUrlGenerator: HttpUrlGenerator) {
        super('bizisp', http, httpUrlGenerator);

    }

    getAll(): Observable<BizISP[]> {
        return this.http.get('/api/bizisps')
            .pipe(
                map(res => res["payload"])
            );
    }

    getAPICounts(): Observable<number> {
        return this.http.get('/api/counts').pipe(
            map(res => res["getapibizisps"] ? res["getapibizisps"] : 1)
        );
    }

}
