import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { BizispsCardListComponent } from './bizisps-card-list/bizisps-card-list.component';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { MatToolbarModule } from '@angular/material/toolbar';
import { NgMatSearchBarModule } from 'ng-mat-search-bar';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { BarRatingModule } from "ngx-bar-rating";
import { RouterModule, Routes } from '@angular/router';
import { EntityDataService, EntityDefinitionService, EntityMetadataMap } from '@ngrx/data';
import { compareBizisps, BizISP } from './model/bizisp';
import { BizispEntityService } from './services/bizisp-entity.service';
import { BizispsResolver } from './services/bizisps.resolver';
import { BizispsDataService } from './services/bizisps-data.service';
import { BizispviewComponent } from './bizispview/bizispview.component';
import { MatChipsModule } from '@angular/material/chips';

export const bizispsRoutes: Routes = [
    {
        path: '',
        component: HomeComponent,
        resolve: {
            bizisps: BizispsResolver
        }
    }
];

const entityMetadata: EntityMetadataMap = {
    bizisp: {
        sortComparer: compareBizisps,
        entityDispatcherOptions: {
            optimisticUpdate: true
        }
    }
};


@NgModule({
    imports: [
        CommonModule,
        MatToolbarModule,
        MatButtonModule,
        MatDialogModule,
        MatIconModule,
        MatCardModule,
        MatListModule,
        MatInputModule,
        MatChipsModule,
        MatSelectModule,
        NgMatSearchBarModule,
        BarRatingModule,
        ReactiveFormsModule,
        RouterModule.forChild(bizispsRoutes)
    ],
    declarations: [
        HomeComponent,
        BizispsCardListComponent,
        BizispviewComponent
    ],
    exports: [
        HomeComponent,
        BizispsCardListComponent
    ],
    providers: [
        BizispEntityService,
        BizispsResolver,
        BizispsDataService
    ]
})
export class BizispsModule {
    constructor(
        private eds: EntityDefinitionService,
        private entityDataService: EntityDataService,
        private bizispsDataService: BizispsDataService) {
        eds.registerMetadataMap(entityMetadata);
        entityDataService.registerService('bizisp', bizispsDataService);
    }
}
