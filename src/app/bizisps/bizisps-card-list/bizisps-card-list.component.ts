import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { BizISP } from "../model/bizisp";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { BizispEntityService } from '../services/bizisp-entity.service';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';

@Component({
  selector: 'bizisps-card-list',
  templateUrl: './bizisps-card-list.component.html',
  styleUrls: ['./bizisps-card-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BizispsCardListComponent implements OnInit {

  @Input()
  bizISPs: BizISP[];

  @Output()
  onChangeSelectedISP = new EventEmitter();

  constructor(
    private dialog: MatDialog,
    private store: Store<AppState>,
    private bizISPService: BizispEntityService) {
  }

  ngOnInit() {

  }

  editCourse(course: BizISP) {

    /*        const dialogConfig = defaultDialogConfig();
    
            dialogConfig.data = {
              dialogTitle:"Edit Course",
              course,
              mode: 'update'
            };
    
            this.dialog.open(EditCourseDialogComponent, dialogConfig)
              .afterClosed()
              .subscribe(() => this.courseChanged.emit());*/

  }
  onShowISPDetail(event, isp: BizISP) {
    this.onChangeSelectedISP.emit(isp);
  }
  getBasePlan(currentISP: BizISP) {
    return currentISP.plans.reduce((a: any, b: any) => {
      return a.price < b.price ? a : b;
    });
  }
  onDeleteCourse(course: BizISP) {

    this.bizISPService.delete(course)
      .subscribe(
        () => console.log("Delete completed"),
        err => console.log("Deleted failed", err)
      );


  }

}









