import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { BizISP } from '../model/bizisp';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { BizispEntityService } from '../services/bizisp-entity.service';
import { BizispsDataService } from '../services/bizisps-data.service';


@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent implements OnInit {
  eventsSubject: Subject<BizISP> = new Subject<BizISP>();
  ispTotal$: Observable<number>;
  apiCount$: Observable<number>;
  bizISPList$: Observable<BizISP[]>;
  beginnerCourses$: Observable<BizISP[]>;
  advancedCourses$: Observable<BizISP[]>;

  constructor(private bizISPEntityService: BizispEntityService,
    private bizAPIDataService: BizispsDataService) {

  }
  ngOnInit() {
    this.bizISPList$ = this.bizISPEntityService.entities$;
    this.ispTotal$ = this.bizISPEntityService.entities$
      .pipe(
        map(courses => courses.length)
      );
    this.apiCount$ = this.bizAPIDataService.getAPICounts();
    console.log(this.apiCount$);
  }
  onChangeSelectedISP(selectedISP: BizISP) {
    this.eventsSubject.next(selectedISP);

  }
  emitEventToChild() {
    this.eventsSubject.next();
  }
}
