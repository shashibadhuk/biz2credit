
export interface BizISP {
  id: number;
  seq: number;
  name: string,
  description?: string,
  logo: string;
  averagerating: string;
  courseListIcon: string;
  contacts: {
    phone: string,
    email: string,
    website: string
  };
  plans: any;
}

export function compareBizisps(c1: BizISP, c2: BizISP) {

  const compare = c1.seq - c2.seq;

  if (compare > 0) {
    return 1;
  }
  else if (compare < 0) {
    return -1;
  }
  else return 0;

}
