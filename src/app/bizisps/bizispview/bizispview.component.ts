import { Component, OnInit, Input } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { BizISP } from '../model/bizisp';

@Component({
  selector: 'bizisp-view',
  templateUrl: './bizispview.component.html',
  styleUrls: ['./bizispview.component.css']
})
export class BizispviewComponent implements OnInit {
  private eventsSubscription: Subscription;
  selectedISP: BizISP;
  @Input() events: Observable<BizISP>;
  constructor() { }

  ngOnInit() {
    this.eventsSubscription = this.events.subscribe((ISP: BizISP) => {
      this.selectedISP = ISP;
    });
  }
  getBasePlan() {
    return this.selectedISP.plans.reduce((a: any, b: any) => {
      return a.price < b.price ? a : b;
    });
  }

  ngOnDestroy() {
    this.eventsSubscription.unsubscribe();
  }
}
