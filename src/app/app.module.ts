import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';

import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { StoreModule } from '@ngrx/store';

import { EffectsModule } from '@ngrx/effects';
import { metaReducers, reducers } from './reducers';
import { EntityDataModule } from '@ngrx/data';


const routes: Routes = [
    {
        path: 'bizisps',
        loadChildren: () => import('./bizisps/bizisps.module').then(m => m.BizispsModule)
    },
    {
        path: '**',
        redirectTo: 'bizisps'
    }
];


@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        RouterModule.forRoot(routes),
        HttpClientModule,
        MatIconModule,
        StoreModule.forRoot(reducers, {
            metaReducers,
            runtimeChecks: {
                strictStateImmutability: true,
                strictActionImmutability: true,
                strictActionSerializability: true,
                strictStateSerializability: true
            }
        }),
        EffectsModule.forRoot([]),
        EntityDataModule.forRoot({})
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
